
# TODO - urgent!!!
* Finalize all plots and make them part of simulation (Adding the value of the objective function is also a very, very smart idea to get a sense of what is going on)
* Add very, very basic model (I.e basic controller with Brute force optimization)
* Finalize simple baseline (i.e brute force) and add figures to beamer
* Finalize baseline based on doctor (and add figures to the beamer)
* Add controller stuff


###### INSULIN_TO_CARB_RATIO -> "Guess it" 20. Meal/ICR, 
###### Use previous guess, if you are high, then correct for that you are high - give more. Insulin sensittiivty factor (ISF)
###### Meal/ICR + (G-Gbar)/ISF (mmol/L/U), set it to 2

# TODO - later (not urgent)
* Finalize CVGA plot (All patients)
* Finalize CVGA plot (CI)

# Quick question

# Overleaf thesis document
https://www.overleaf.com/9479384223vzzsqndgjnhn

# Relevant links on MPC
* https://github.com/lucasrm25/Gaussian-Process-based-Model-Predictive-Control
* https://sites.engineering.ucsb.edu/~jbraw/mpc/
* http://cse.lab.imtlucca.it/~bemporad/mpc_course.html
* https://www.syscop.de/teaching/ss2017/numerical-optimal-control
* https://mariozanon.wordpress.com/numerical-methods-for-optimal-control/

# Markdown cheatsheet
* https://wordpress.com/support/markdown-quick-reference/


# Anaconda cheatsheet
* conda info --envs
  * Get list of environments 

# Jupyter commands to save

## Cheatsheet
* sxm2sh 
  * GPU cluster with the best CPU (and GPU)
* voltash
  * GPU cluster with cpu (not many use)
* qrsh
  * Basic CPU cluster

## Commands on your machine
alias hpc_jupyter="ssh -N -L 8080:n-62-20-10:8080 s164180@login2.gbar.dtu.dk"

alias hpc="ssh s164180@login2.gbar.dtu.dk"

hpc_sync_to_remote ()
{
  rsync -av "$1" "s164180@transfer.gbar.dtu.dk:""$2"
}

hpc_sync_from_remote ()
{
  rsync -av "s164180@transfer.gbar.dtu.dk:""$2" "$1"
}

## Commands on the cluster
jupyter notebook --no-browser --port=8080 --ip=$HOSTNAME


## Junk code
'''

# Sanity check - patient 2
u_opt = np.array([0.000000000000000000e+00,4.070452119136734837e+01,8.048792359326273527e+01,1.193944136520972421e+02,1.574693638039242103e+02,
1.919220624032691660e+02,2.206978386535250252e+02,2.476647788623437805e+02,2.729091381798292559e+02,2.970725340845122560e+02,
3.203361818558191771e+02,3.428635841740747878e+02,3.647323727326320864e+02,3.860009840994162573e+02,
4.066616368090938067e+02,4.268006887310402249e+02,4.464553275003594308e+02,4.656522747712259047e+02,4.844170274063920374e+02,5.027724507881289924e+02,5.207400196766277531e+02])

plt.figure()
CBO.plot_action_context_space(action_space = bolus_range,
                                    context_space = meal_range,
                                    plot_ideal=True,
                                    ideal_c=range(0,21),
                                    ideal_x=u_opt,
                                    xlabel='Bolus Insulin [U]',
                                    ylabel='Meal CHO [g/5min]')
plt.savefig('sanity.png')
plt.show()
'''