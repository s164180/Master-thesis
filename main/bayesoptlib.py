import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import minimize
from scipy.stats import norm
import matplotlib.gridspec as gridspec
from matplotlib import cm
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D

from sklearn.preprocessing import MinMaxScaler 
scaler = MinMaxScaler()

import GPy

class contextual_bayesian_optimization:
    # Written by: Dinesh Krishnamoorthy, July 2020

    def __init__(self,X_sample,Y_sample,context,bounds,
                          kernel,
                          model,
                          X_grid = np.linspace(0,1,100)):
        self.X_sample = X_sample  # X - combined action-context space
        self.Y_sample = Y_sample  # Observed objective function
        self.context = context  # New context
        self.bounds = bounds
        self.kernel = kernel
        self.X_grid = X_grid
        self.grid_size = self.X_grid.shape[0]
        self.nX = self.X_sample.shape[1] 
        self.nC = self.context.shape[1]
        self.nU = self.nX - self.nC
        self.C_grid = self.context*np.ones([self.grid_size,self.nC]).reshape(-1,self.nC)

        self.m = model
    
    def posterior(self):
        testX = np.concatenate((self.X_grid.reshape(-1,self.nU),self.C_grid),axis=1)
        mu, sigma = self.m.predict(testX)

        return mu, sigma
    
    def extract_action_space(self):
        nU = self.nX-self.nC
        return np.array(self.X_sample[:,0:nU+1]).reshape(-1,nU)
    
    def query_next_TS(self): # Thompson Sampling
        
        testX = np.concatenate((self.X_grid.reshape(-1,self.nU),self.C_grid),axis=1)
        posterior_sample = self.m.posterior_samples_f(testX,full_cov=True,size = 1).reshape(-1,1)
        
        min_index = np.argmin(posterior_sample)
        U_next = self.X_grid[min_index]
        self.X_next = np.concatenate((U_next.reshape(-1,self.nU),self.context),axis=1) #np.array([U_next,self.context]).ravel()
        return self

    def EI(self,X,xi=0.01):
        mu, sigma = self.m.predict(X.reshape(-1,self.nX))
        mu_sample,si = self.m.predict(self.X_sample)
        f_best = np.max(-mu_sample) # incumbent
        with np.errstate(divide='warn'):
            imp = mu_sample 
            Z = imp/sigma
            EI = (imp*norm.cdf(Z) + sigma*norm.pdf(Z))
            EI[sigma == 0.0] = 0.0
        return EI

    def query_next_EI(self):
        testX = np.concatenate((self.X_grid.reshape(-1,self.nU),self.C_grid),axis=1)
        EI = self.EI(testX)
        min_index = np.argmax(EI)
        U_next = self.X_grid[min_index]
        self.X_next = np.concatenate((U_next.reshape(-1,self.nU),self.context),axis=1) #np.array([U_next,self.context]).ravel()
        return self
    
    def query_next_UCB(self): #Upper confidence bound 
        
        testX = np.concatenate((self.X_grid.reshape(-1,self.nU),self.C_grid),axis=1)
        mu, sigma = self.m.predict(testX)
        self.LCB = mu - 2*sigma
        min_index = np.argmin(self.LCB)
        U_next = self.X_grid[min_index]
        self.X_next = np.concatenate((U_next.reshape(-1,self.nU),self.context),axis=1) #np.array([U_next,self.context]).ravel()
        return self

    def query_next_greedy(self): # Greedy policy

        testX = np.concatenate((self.X_grid.reshape(-1,self.nU),self.C_grid),axis=1)
        mu, sigma = self.m.predict(testX)
        self.greedy = mu
        min_index = np.argmin(self.greedy)
        U_next = self.X_grid[min_index]
        self.X_next = np.concatenate((U_next.reshape(-1,self.nU),self.context),axis=1) #np.array([U_next,self.context]).ravel()
        return self

    def LCB(self,X):  
        testX = np.concatenate((X.reshape(-1,self.nU),self.context),axis=1)
        mu, sigma = self.m.predict(testX.reshape(-1,self.nX))
        return mu - 1/2*sigma

    def query_next(self,acquisition='LCB'):
        nU = self.nX - self.nC

        min_val = -1e-5
        min_x = self.X_sample[-1,:] # Latext X
        n_restarts=25

        def min_obj(X,self):
            if acquisition=='LCB' or acquisition=='UCB' :
                alpha = self.LCB(X)
            elif acquisition == 'expect':
                alpha = self.expect(X)
            return alpha

        # Find the best optimum by starting from n_restart different random points.
        for x0 in np.random.uniform(self.bounds[:, 0].T, self.bounds[:, 1].T,
                                    size=(n_restarts, nU)):
            res = minimize(min_obj, x0=x0, args = (self),
                           bounds=self.bounds, method='L-BFGS-B')        
            if res.fun < min_val:
                min_val = res.fun[0]
                min_x = res.x           

        U_next = min_x.reshape(-1, nU)
        self.X_next = np.concatenate((U_next.reshape(-1,self.nU),self.context),axis=1)
        self.min_val = min_val
        return self


class exploit:
    def __init__(self,model,context,bounds):
        self.model = model
        self.context = context
        self.bounds = bounds
        
    
    def compute_optimum(self):
        nC = self.context.shape[1]    
        nU = self.bounds.shape[0] 
        
        min_val = 100000000
        min_x = None
        n_restarts=25
        
        def min_obj(X,self):
            mu, sigma = self.model.predict(np.array([X,self.context]).T)
            return mu
        
        # Find the best optimum by starting from n_restart different random points.
        for x0 in np.random.uniform(self.bounds[:, 0].T, self.bounds[:, 1].T,
                                    size=(n_restarts, nU)):
            res = minimize(min_obj, x0=x0, args = (self),
                           bounds=self.bounds, method='L-BFGS-B')        
            if res.fun < min_val:
                min_val = res.fun[0]
                min_x = res.x           

        U_next = min_x.reshape(-1, nU)
        return U_next
    
    def compute_optimum_1(self):
        nC = self.context.shape[1]    
        nU = self.bounds.shape[0] 

        U_grid = np.linspace(self.bounds[:, 0].T, self.bounds[:, 1].T, 100).reshape(-1,nU)
        C_grid = self.context*np.ones([100,1])
        testX = np.concatenate((U_grid,C_grid),axis=1)

        mu, sigma = self.model.predict(testX)
        GP_mean = mu
        min_index = np.argmin(GP_mean)
        U_next = U_grid[min_index]

        return U_next

