#!/usr/bin/env python
# coding: utf-8

# In[3]:


from DiabetesModels import *

# Parameters for simulation
environment = read_env(ndim=2)

# Setup simulation from env dict
simulation = Simulation(environment._asdict())
# Get idea of paars to change
environment

# Turn notebook into script
# !jupyter nbconvert --to script FILENAME.ipynb
# !mv FILENAME.ipynb simulations/


# In[2]:


# Change parameters of simulation here if you want to
# look above!
# simulation.par = xxx
# simulation.patientNr = 851
simulation.loss_fun = 'CGP'
simulation.hypoglycemia = 5.0
simulation.target = 5.0
## Initiate simulation for patient

# print environment for debugging purposes
simulation_tuple = namedtuple('debug_tuple', simulation.__dict__)
print(simulation_tuple(**simulation.__dict__))


# In[3]:


### Init patient
simulation.init_patient()


# In[4]:


# Define sumulation function
meal_range = np.linspace(5,20,40)
bolus_range = np.linspace(0,4000, 400)

sim_fun = simulation.calculate_obj_fun_grid


# In[5]:


# Check if simulation is done and then do preprocessing
simulation.simulate(sim_fun, meal_range = meal_range, bolus_range = bolus_range)

