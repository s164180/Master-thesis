#!/usr/bin/env python
# coding: utf-8

# In[1]:


from DiabetesModels import *

# Parameters for simulation
environment = read_env(ndim=2)

# Setup simulation from env dict
simulation = Simulation(environment._asdict())
# Get idea of paars to change
environment

# Turn notebook into script
# !jupyter nbconvert --to script config_template.ipynb


# In[2]:


# Change parameters of simulation here if you want to
# look above!
# simulation.par = xxx
# simulation.patientNr = 851
simulation.loss_fun = 'CGP'
simulation.hypoglycemia = 5.0
simulation.target = 5.0
## Initiate simulation for patient

# print environment for debugging purposes
simulation_tuple = namedtuple('debug_tuple', simulation.__dict__)
print(simulation_tuple(**simulation.__dict__))


# In[3]:


### Init patient
simulation.init_patient()


# In[38]:


# Define sumulation function
u_opt_meal_range = np.linspace(0,20,100)
sim_fun = simulation.calculate_u_opt


# In[39]:


# Check if simulation is done and then do preprocessing
simulation.simulate(sim_fun, u_opt_meal_range = u_opt_meal_range)


# In[40]:


##.... This does actually look fine

if simulation.jupyter:
    patient = simulation.patient
    plt.figure()
    plt.plot(patient.u_opt_meal_range, patient.u_opt, '-*')
    plt.plot(patient.u_opt_meal_range, patient.u_opt_meal_range/patient.ICR, '-*')
    plt.show()

    plt.plot(patient.ICR_it)


# In[ ]:




