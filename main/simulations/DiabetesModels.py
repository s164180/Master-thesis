import numpy as np
from scipy.integrate import odeint
from scipy.optimize import fsolve
import Hovorka 
import numpy.matlib
import matplotlib.pyplot as plt
from scipy import interpolate

from tqdm import tqdm
from scipy.optimize import minimize
from sklearn.linear_model import LinearRegression
from scipy.interpolate import interp1d
import sys
import GPy

from scipy.stats import norm


from utils import *

class Simulation(object):
    # Written by: Qahir Yousefi, Nov 2021
    def __init__(self, env):
        # self.env = env
        self.__dict__.update(env)

    def calculate_obj_fun_grid(self, meal_range, bolus_range):

        # Do this to initiate
        try:
            self.obj_fun_meal_grid
        except:
            self.obj_fun_meal_grid = meal_range
            self.obj_fun_bolus_grid = bolus_range
            self.obj_fun_grid = np.zeros((len(meal_range), len(bolus_range)))

        for i, meal in enumerate(tqdm(meal_range, desc = f'calculating obj_fun for patientNr {self.patientNr}')):
            for j, bolus in enumerate(bolus_range):
                self.patient.simulate(Bolus=bolus, Meal = meal, x0 = self.patient.xSS)
                self.obj_fun_grid[i,j] = self.patient.loss()
        
        self.files_to_save['obj_fun_meal_grid'] = self.obj_fun_meal_grid
        self.files_to_save['obj_fun_bolus_grid'] = self.obj_fun_bolus_grid
        self.files_to_save['obj_fun_grid'] = self.obj_fun_grid
    
    def init_patient(self):
    
        self.files_to_save = dict()

        # Initiate and simulate patient to obtain inital sample
        self.patient = Patient(env = self.__dict__)

        Meal = np.array([10]).reshape((1,1))
        Bolus = Meal/self.patient.ICR
        # historical data - use Doctor's
        if self.ndim == 2:
            self.X_sample = np.array([0,0]).reshape(1,2)
        elif self.ndim == 3:
            # self.X_sample = np.array([Bolus,Meal,self.target]).reshape(1,3)
            self.X_sample = np.array([0,0,self.target]).reshape(1,3)

        self.patient.simulate(self.X_sample[0][0],self.X_sample[0][1],x0 = self.patient.xSS)

        self.Y_sample = np.array(self.patient.loss()).reshape(1,1)

        # Preallocate memory
        self.nTAR = np.zeros(self.nMeals)
        self.nTIR = np.zeros(self.nMeals)
        self.nTBR = np.zeros(self.nMeals)
        self.nMeal = np.zeros(self.nMeals)
        self.nBG0 = np.zeros(self.nMeals)
        self.nCGM = np.zeros((self.nMeals, self.patient.nSim))

        self.xinit = self.patient.xSS

    def sample_meal(self, i):
        Meal = np.random.uniform(5,20, size = (1,1))
        return  Meal
    
    def simulate_i(self, Bolus, Meal, i):
        # Change X_next based on if we are in 2D or 3D
        if self.ndim == 3:
            X_next = np.array([Bolus.squeeze(), Meal.squeeze(), self.patient.CGM[-1]]).reshape(1,self.ndim)
        else:
            X_next = np.array([Bolus.squeeze(), Meal.squeeze()]).reshape(1,self.ndim)

        # Simulate and obtain lsos function
        self.patient.simulate(X_next[0][0],X_next[0][1],x0 = self.xinit)
        Y_next = self.patient.loss()
        
        self.X_sample = np.vstack((self.X_sample, X_next))
        self.Y_sample = np.vstack((self.Y_sample, Y_next))

        # Other things to save
        CGMi = self.patient.CGM
        x = np.where(CGMi<=3.9)
        y = np.where(CGMi>10)
        TAR = len(y[0])/len(CGMi)*100
        TBR = len(x[0])/len(CGMi)*100
        TIR = 100 - TAR - TBR

        self.nTAR[i] = TAR
        self.nTIR[i] = TIR
        self.nTBR[i] = TBR
        self.nMeal[i] = Meal
        self.nCGM[i,:] = CGMi
        self.nBG0[i] = self.xinit[10]

        # Set next
        self.xinit = self.patient.xEnd
        if self.ndim == 2:
            self.xinit = self.patient.xSS
    
    def simulate_meals(self, bolus_fun):
        for i in tqdm(range(self.nMeals), desc = f'Simulating for PatientNr: {self.patientNr}'):
            np.random.seed(int(i)) # VERY important that this is HERE!!!!

            Meal = self.sample_meal(i)

            Bolus = bolus_fun(Meal, self)
            
            self.simulate_i(Bolus, Meal, i)

        # Save after simulation is done
        self.files_to_save['nTAR'] = self.nTAR
        self.files_to_save['nTIR'] = self.nTIR
        self.files_to_save['nTBR'] = self.nTBR
        self.files_to_save['nMeal'] = self.nMeal
        self.files_to_save['nCGM'] = self.nCGM
        self.files_to_save['nBG0'] = self.nBG0
        self.files_to_save['X_sample'] = self.X_sample
        self.files_to_save['Y_sample'] = self.Y_sample        
    
    def debug_plot(self):
        self.plot_CGM(f'{self.path}debug/CGM{self.patientNr}.pdf')
        self.make_u_opt_plot(f'{self.path}debug/u_opt{self.patientNr}.pdf')
    
    def simulate(self, sim_fun, **kwargs):
        #while self.patientNr < self.maxPatient:
        sim_fun(**kwargs)

        #If we are in jupyter break since we do not want to update
        self.save()

        # Check if simulation is actually done to preprocess files
        if simulation_is_done(self.path, self.maxPatient):
            print(f'Preprocessing all simulation data - current patientNr is: {self.patientNr}')
            preprocess_simulation_data(self.path) # preprocess output of all simulations
    
    def calculate_u_opt(self, u_opt_meal_range, x0 = None):
            self.patient.calculate_u_opt(u_opt_meal_range)
            self.files_to_save['u_opt'] = self.patient.u_opt
            self.files_to_save['u_opt_meal_range'] = self.patient.u_opt_meal_range
    
    def plot_CGM(self, figName = None):
        plt.figure(figsize=(10,7))
        time = np.linspace(0,self.patient.nSim,self.patient.nSim)*self.patient.Ts/60
        plt.fill_between(time,3.9+0*time,7+0*time,color='g',alpha=0.9)
        plt.fill_between(time,0+0*time,3.9+0*time,color='r',alpha=0.9)

        for i in range(self.nMeals):
            plt.plot(time,self.nCGM[i,:],color=(0.7,0.0,0.0),linewidth=2, alpha =0.1)

        plt.xlabel('Time since meal [h]')
        plt.ylabel('CGM [mmol/L]')
        # plt.ylim(1,25)
        plt.title(f'PatientNr = {self.patient.patientNr}, mean CGM at 6 hr = {np.mean(self.nCGM,axis=0)[-1]:.2f} mmol/L')
        plt.legend()
        if figName:
            plt.savefig(figName, dpi = 150, bbox_inches='tight')
        plt.show()
    
    def make_u_opt_plot(self, figName = None, method = None):
        #### We need to compare with the optimal

        plt.figure(figsize=(10,7))
        plt.plot(self.patient.u_opt_meal_range, self.patient.u_opt, '-*', label = 'Numerical interpolation (2D)', alpha = 0.5)
        # plt.plot(self.patient.u_opt_meal_range, self.patient.u_opt_meal_range * 20, '-g', label = '2')
        plt.plot(self.patient.u_opt_meal_range, self.patient.u_opt_meal_range/self.patient.ICR, '-g', label = 'KN')
        plt.plot(self.X_sample[:,1], self.X_sample[:,0], 'r.', label = f'{method}')
        plt.xlabel('Meal size [g/min]')
        plt.ylabel('Bolus size [mU/min]')
        plt.title(f'Plot of bolus as a size of meal size for patientNr: {self.patient.patientNr} ')
        plt.legend()
        plt.show()
    
    def save(self):
            print(f'Saving files for {self.patientNr}:')
            save_files(self.files_to_save, path=self.path, patientNr=self.patientNr)

            # load last patient
            #self.patientNr = update_lastPatient(self.path, self.maxPatient)

             # Fix bug
            #if self.patientNr < self.maxPatient:
                #self.init_patient()

class Patient:
    # Written by: Dinesh Krishnamoorthy, Sep 2021

    def __init__(self,env):

        self.__dict__.update(env) # This way to get all parameters of simulation

        # Initialize Bayesian Optimization

        # Go from 100 to 3000

        # Initiation functions

        # self.n_callibration = 20

        self.parm = Hovorka.HovorkaRandom(self.patientNr)

        # initiate steady state and ISF
        self.steadyState()

        # Print steady basal
        print(f'Basal rate found to be {self.basal} [mU/min] for patientNr {self.patientNr}', file = sys.stderr)

        self.init_ISF()

        # Initiate number of dimension and scaling constant for prior ---> Do not do this!
        # self.scaling_constant = self.initiate_scaling_constant_prior()

        # Set u_opt
        self.set_u_opt()

        self.ICR_TRUE = self.estimate_ICR(self.u_opt, self.u_opt_meal_range)

        # Initiate ICR in crude manner
        self.initiate_ICR_crude()

        #self.ICR = self.ICR_TRUE

        # set scale constant
        self.meal_bias_C = self.set_meal_bias_C()

        # set approx loss
        self.set_approx_loss()

    def init_ISF(self):
        '''
        Estimate ISF of patient
        '''
        Bolus = 100 # We give 0.1 U corresponding 100 mU

        self.simulate(Bolus = Bolus, Meal = 0, x0 = self.xSS, nsimHours = 24)

        ISF = (self.target - min(self.CGM))/0.1 # Here we use 0.1 U to get back to proper units!
        self.ISF = ISF # the ISF is in mmol/L/U !!!

    def set_u_opt(self):
        if self.loss_fun == 'PPP':
            path_to_u_opt = f'{self.path_to_simulations}calculate_u_opt_PPP/'
        elif self.loss_fun == 'CGP':
            path_to_u_opt = f'{self.path_to_simulations}calculate_u_opt_CGP/'

        dat = np.load(f'{path_to_u_opt}data/patient{self.patientNr}.npz')
        self.u_opt = dat['u_opt']
        self.u_opt_meal_range = dat['u_opt_meal_range']
    
    def set_meal_bias_C(self):
        f = self.CGP_loss()
        self.f = f

        if self.ndim == 2:
            x0 = np.array([10/self.ICR,10]).reshape((1,2))
        else:
            x0 = np.array([10/self.ICR,10, 6]).reshape((1,3))

        self.simulate(Bolus = 15/self.ICR, Meal = 15, x0 = self.xSS)

        meal_bias_C = f/(x0[:,1]**2)

        return meal_bias_C

    def distance_term(self, x):
        m = 1/self.ICR

        if self.ndim == 2:
            y =  self.scale_constant * (x[:,0] - x[:,1]*m)
        else:
            y = self.scale_constant * (x[:,0] - x[:,1]*m) # Add some kind of way to "change directions here"
        y = (y/m) ** 2

        return y.reshape(-1,1)
    
    def meal_bias_term(self, x):
        y = self.meal_bias_C *  (x[:,1]**2)

        return y.reshape(-1,1)

    def prior_uniform(self, x):
        m = 1/self.ICR

        upper_bolus = np.maximum((x[:,1] + 5) *m, 20*m)
        lower_bolus =(x[:,1]- 5)*m

        if lower_bolus < 0:
            lower_bolus = 0

        support = x[:,0] * 0
        support[(x[:,0] < lower_bolus)] = 100000
        support[(x[:,0] > upper_bolus)] = 100000

        y = support.reshape(-1,1) + self.meal_bias_term(x)

        return y.reshape(-1,1)

    def prior_2d_experiment(self, x):
    
        y = self.distance_term(x) + self.meal_bias_term(x)

        # Restrict 
        return y.reshape(-1,1)
    
    def CGM_bias_term(self, x):
        y = 6*60/5*(x[:,2]-self.target)**2
        return y.reshape(-1,1)

    def prior_3d_experiment(self, x):
        y = self.prior_2d_experiment(x) + self.CGM_bias_term(x)

        return y.reshape(-1,1)

    def steadyState(self):
        # compute steady-state

        # Goals: Convergence and basal rate above 0.4 xU/hour 
        # Corresponding to 0.4 * 1000/60 mU/min

        tries = 0
        while True:
            x0 = np.zeros((11))
            Gb = self.target  # Target Blood Glucose [mmol/L]
            # xSS, _, ier, _ = fsolve(Hovorka.HovorkaModelWrap,x0,args=(self.parm,Gb), xtol = 1e-12, full_output = True)
            xSS, _, ier, _ = fsolve(Hovorka.HovorkaModelWrap,x0,args=(self.parm,Gb), xtol = 1e-12, full_output = True)

            X = xSS.copy()

            self.basal = xSS[3] # Basal Insulin that maintains nominal BG
            xSS[3] = Gb*self.parm.Vg
            self.xSS = xSS

            # Check conditions
            if self.basal > 0.4 * 1000/60 and ier == 1 and self.parm.it < 1000 and np.sum(X>=0) == len(X): # ier is 1 if convergence, we want at least 0.4 U/hour
                break
            elif not self.patient_filter:
                break
            else:
                tries = tries + 1
                self.parm = Hovorka.HovorkaRandom(self.patientNr + tries * 1000) # Try new parameters
        
        self.tries = tries
        return self
    
    def cubic_interpolation(self, meal_range, u_opt):
        '''
        Do cubic interpolation
        '''
        f = interp1d(meal_range, u_opt.squeeze(), kind = 'cubic') # imported in utils

        return f

    def initiate_ICR_crude(self, ICR0 = 0.04747534505908509): # Initilal ICR used for doctor algorithm, corresponding to q = 0.025)
        # Initial crude step-size for ICR
        self.ICR = ICR0

        self.ICR_it = [self.ICR]

        # Define X callibration and y callibration!
        if self.ndim == 2:
            self.X_callibration = np.array([0,0]).reshape(1,2)
        elif self.ndim == 3:
            # self.X_sample = np.array([Bolus,Meal,self.target]).reshape(1,3)
            self.X_callibration = np.array([0,0,self.target]).reshape(1,3)

        self.Y_callibration = np.array(self.loss()).reshape(1,1)

        for i in range(self.n_callibration):
            Meal = np.array([(i+1)]).reshape((1,1)) * 5 # Integrate over 5 min interval by multiplying with 5 [g/min] -> [g]
            Bolus = Meal/self.ICR # Bolus here is in units  [g/mU]!Bol

            self.simulate(Bolus/5, Meal/5, x0 = self.xSS)

            Bolus_scaled = Bolus/5
            Meal_scaled = Meal/5

            if self.ndim == 3:
                X_next = np.array([Bolus_scaled.squeeze(), Meal_scaled.squeeze(), self.CGM[-1]]).reshape(1,self.ndim)
            else:
                X_next = np.array([Bolus_scaled.squeeze(), Meal_scaled.squeeze()]).reshape(1,self.ndim)

            # Simulate and obtain lsos function
            Y_next = self.loss()
        
            self.X_callibration = np.vstack((self.X_callibration, X_next))
            self.Y_callibration = np.vstack((self.Y_callibration, Y_next))

            #self.simulate(Bolus/5,Meal/5,x0 = self.xSS) # Divide by 5 to obtain rates!
            e = (self.CGM[-1] - self.target)/self.ISF # [(mmol/L) / (mmol/L/U)]
            self.ICR = ((Meal * self.ICR * 1000)/(self.ICR * e * 1000 + Meal)).item()/1000 # the 1000 are for converting units properly

            self.ICR_it.append(self.ICR)
        
        print(f'Crude ICR estimated to be {self.ICR}')

        return self

    def estimate_ICR(self, u_opt, meal_range):
        '''
        Estimates ICR using u_opt and meal_range
        '''

        # bolus = meal/ICR = > bolus * ICR = meal
        reg = LinearRegression(fit_intercept=False).fit(X=u_opt, y = meal_range)
        ICR = reg.coef_[0]
        return ICR

    
    def calculate_u_opt(self, u_opt_meal_range, x0 = None):
        '''
        Calculates u_opt starting at x0, if x0 is none uses xSS
        '''

        # Let default be steady_state
        bounds = np.array([[0,np.inf]]) # no upper bound this might vary patient-by-patient!

        if type(x0) == type(None):
            x0 = self.xSS

        u_opt = []
        u0 = u_opt_meal_range[0]/self.ICR

        for meal in tqdm(u_opt_meal_range, desc = f'Calculating u_opt for patient {self.patientNr}'):
            res = minimize(self.simulate_obj_fun, x0=u0,
                args = (meal, x0, self.loss_fun), # Have to do it this way....
                bounds = bounds, method = 'L-BFGS-B')
            u_opt.append(res.x)
            u0 = res.x # faster convergence
        
        self.u_opt = np.array(u_opt)
        self.u_opt_meal_range = np.array(u_opt_meal_range)
        return self

    def simulate_obj_fun(self, Bolus, Meal, x0, obj_fun):
        self.simulate(Bolus, Meal, x0)
        if obj_fun == 'PPP':
            return self.PPP_loss()
        elif obj_fun == 'CGP':
            return self.CGP_loss()


    def simulate(self,Bolus,Meal,x0,nsimHours = 6,Ts = 5, set_end_points = True):
        """
        Simulates the effect of meal and bolus insulin on a Hovorka virtual patient, 
        and returns the cumulative cost of glycemic variations.

        Written by: Dinesh Krishnamoorthy, June 2020
        
        Input Arguments:
            Bolus [mU]
            Meal [g CHO/ 5 min] (an input of 10 is equal to a 50g meal)
            Initial state x0 
        
        Output:
            Cumulative Glycemic penalty 
            \sum J = 0.5*(BG-6)**2 + 100*0.5*min(BG-4.2,0)**2

        Ts = 5 # Sampling time

        """
        Meal_rate = Meal
        Bolus_rate = Bolus

        
        nSim = int(nsimHours*60/Ts) # Simulation duration, in number of samples 
        
        CGMNoise = np.zeros(nSim+1)

        U = np.matlib.repmat(self.basal, nSim, 1) # Basal insulin
        D = np.zeros(shape = (nSim,1))
        

        D[0] = Meal_rate  # Meal [g CHO/min]
        U[0] = U[0] + Bolus_rate

        CGM = np.zeros(nSim)
        BG = np.zeros(nSim)
                
        for i in range(0,nSim):
            t = np.linspace(0,Ts)+(i-1)*Ts
            
            CGM[i] = x0[10] + CGMNoise[i]
            BG[i] = x0[3]/self.parm.Vg

            x = odeint(Hovorka.HovorkaModel,x0,t,args=(self.parm,U[i],D[i]))
            x0 = x[-1]
        
        # Set end points
        if set_end_points:
            self.xEnd = x0
            self.CGM = CGM
            self.BG = BG
            self.nSim = nSim
            self.Ts = Ts
        
        return CGM


    
    def loss(self,**kwargs):
        if self.loss_fun == 'PPP':
            return self.PPP_loss()
        elif self.loss_fun == 'CGP':
            return self.CGP_loss()
    
    def PPP_loss(self, peakC = 20, targetC = 1, valleyC = 50, target = None, hypoglycemia = None):
        if target is None:
            target = self.target
        if hypoglycemia is None:
            hypoglycemia = self.hypoglycemia

        # Dinesh param: peakC = 20, valleyC = 50, targetC = 1
        peak = max(self.CGM) - self.CGM[0] # max minus start
        valley = np.array(max(0, self.target - min(self.CGM)))
        target = np.abs(self.CGM[-1] - self.target)

        PPP = peak * peakC + target * targetC + valley * valleyC
        return PPP
    
    def CGP_loss(self, kappa = 100):
        GP = 1/2*(self.CGM - self.target)**2 + 1/2*kappa*np.maximum(self.hypoglycemia - self.CGM, 0)**2 # Calculate GP elementwise
        return GP.sum()

    def plot_CGM(self):
        time = np.linspace(0,self.nSim,self.nSim)*self.Ts/60

        plt.fill_between(time,3.9+0*time,7+0*time,color='g',alpha=0.1)
        plt.fill_between(time,0+0*time,2.7+0*time,color='r',alpha=0.1)
        plt.plot(time,self.CGM,color=(0.7,0.0,0.0),linewidth=2,label='CGM')
        #plt.plot(time,self.BG,color=(0.7,0.0,0.0),linewidth=2,label='BG')
        plt.xlabel('Time since meal [h]')
        plt.ylabel('CGM [mmol/L]')
        plt.ylim(1,20)
        plt.show()

    
    def set_approx_loss(self):
        dat = np.load(f'{self.path_to_simulations}calculate_true_fun/data/patient{self.patientNr}.npz')
        self.obj_fun_meal_grid = dat['obj_fun_meal_grid']
        self.obj_fun_bolus_grid = dat['obj_fun_bolus_grid']
        self.obj_fun_grid = dat['obj_fun_grid']

        f = interpolate.interp2d(self.obj_fun_bolus_grid, self.obj_fun_meal_grid, self.obj_fun_grid, kind='cubic')

        self.approx_loss = f



