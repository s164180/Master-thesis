#!/bin/sh 
cp ../*.py ./ 
rm -rf $2
cp -R setup/ $2
sed -e "s/FILE/$1/g" -e "s/PATH/$2/g" < job.sh | bsub

## Example
## ./sender.sh SCRIPT FOLDER NUM_PATIENTS 
## ./sender.sh run.py run 200 
## Replace: | bsub with: > debug.txt for debugging