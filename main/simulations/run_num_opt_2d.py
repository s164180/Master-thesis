#!/usr/bin/env python
# coding: utf-8

# In[2]:


from DiabetesModels import *

# Parameters for simulation
environment = read_env(ndim=2)

# Setup simulation from env dict
simulation = Simulation(environment._asdict())
# Get idea of paars to change
environment

# Turn notebook into script
# !jupyter nbconvert --to script config_template.ipynb


# In[3]:


get_ipython().system('jupyter nbconvert --to script run_num_opt_2d.ipynb')
get_ipython().system('mv run_num_opt_2d.py simulations/')


# In[4]:


# Change parameters of simulation here if you want to
# look above!
# simulation.par = xxx

if simulation.jupyter:
    simulation.patientNr = 10

# simulation.patientNr = 0
simulation.nMeals = 200
simulation.loss_fun = 'CGP'
simulation.hypoglycemia = 5.0
simulation.target = 5.0
## Initiate simulation for patient

# print environment for debugging purposes
simulation_tuple = namedtuple('debug_tuple', simulation.__dict__)
print(simulation_tuple(**simulation.__dict__))


# In[5]:


### Init patient
simulation.init_patient()
simulation.patient.set_u_opt()


# In[6]:


def bolus_fun_num_opt(Meal, simulation):
    f = simulation.patient.cubic_interpolation(simulation.patient.u_opt_meal_range, simulation.patient.u_opt)

    bolus = f(Meal)

    return bolus


# In[7]:


### Do simulation
simulation.simulate(simulation.simulate_meals, bolus_fun = bolus_fun_num_opt)


# In[8]:


if simulation.jupyter:
    simulation.patient.set_u_opt()
    simulation.plot_CGM()
    simulation.make_u_opt_plot()

    plt.figure()
    plt.plot(simulation.nBG0)
    plt.hlines(6,0,200,'g')
    plt.hlines(5,0,200,'m')
    plt.hlines(4,0,200,'r')
    plt.show()

    print(simulation.nBG0.mean(),simulation.nTIR.mean(), simulation.nTBR.mean())

    plt.figure()
    for i in range(200):
        plt.plot(simulation.nCGM[i,:],color=(0.7,0.0,0.0),linewidth=2,label='CGM', alpha = 0.1 * i/200)
    plt.show()
    
    plt.figure()
    plt.plot(simulation.nMeal * 5,simulation.nTIR, '*', alpha = 0.95)
    plt.show()
    


# In[10]:


simulation.X_sample[-1]


# In[ ]:




