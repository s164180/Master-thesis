from utils import *

# Load env 
env = read_env(jupyter=False)
path, maxPatient,patientNr = env.path, env.maxPatient, env.patientNr

# %%
# Perform optimization
while patientNr < maxPatient:
    files_to_save = dict()

    patient = Sim.Patient(patientNr)

    ### Do optimization here!1!
    patient.steadyState() # compute the basal insulin and steady-state 
    meal_range = np.linspace(0,20,100)
    patient.calculate_u_opt(u_opt_meal_range = meal_range) #calculate u_opt
    u_opt = patient.u_opt
    
    # save files
    files_to_save['u_opt'] = u_opt
    files_to_save['meal_range'] = meal_range
    save_files(files_to_save, path=path, patientNr=patientNr)

    # load last patient
    patientNr = update_lastPatient(path)

# %%
# Check if simulation is done and then do preprocessing
if simulation_is_done(path, maxPatient):
    preprocess_simulation_data(path) # preprocess output of all simulations

