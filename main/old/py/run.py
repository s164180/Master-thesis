from utils import *

# Parameters for simulation

env = read_env()

patientNr = env.patientNr
path = env.path
maxPatient = env.maxPatient

while patientNr < maxPatient:
    # read and update patient number
    patientNr = patientNr + 1
    write_lastPatient(patientNr, path)

    patient = Sim.Patient(patientNr)

    # historical data
    X_sample = np.array([0,0]).reshape(1,2)

    patient = Sim.Patient(patientNr,target=6)
    patient.steadyState() # compute the basal insulin and steady-state 

    patient.simulate(X_sample[0][0],X_sample[0][1],x0 = patient.xSS)
    Y_sample = patient.CGP.reshape((1,1))

    ## find u_opt from files
    u_opt = np.load(file=f'u_opt/data/patient{patientNr}.npz')['u_opt']
    u_opt_meal_range = list(range(0,21))

    patient.estimate_ICR(u_opt=u_opt, meal_range = u_opt_meal_range)
    patient.ICR = patient.ICR * 1.2

    inputDim = 2  # bolus, meal, and initial glucose level
    var0 = 10000
    kernel =  GPy.kern.Matern52(inputDim,ARD=True,lengthscale=[np.std(u_opt),np.std(u_opt_meal_range)],variance=var0)
    mf = GPy.core.Mapping(inputDim,1)

    mf.f = patient.prior
    mf.update_gradients = lambda a,b: None

    bolus_range = np.linspace(0, 4000, 10000)
    meal_range = np.linspace(0, 20, 1000).reshape(50,-1)

    bolus_bounds = np.array([min(bolus_range), max(bolus_range)])

    ## Perform simulation
    k=0
    nTIR = []
    nTBR = []
    nMeal = []
    CGPI = []
    BG0 = []
    nCGPi = []
    xinit = patient.xSS

    nMeals = 200
    al = np.linspace(0,1,len(range(nMeals)))
    fit_hyper_parameters_n_samples = 50

    patient.simulate(0,0,x0 = patient.xSS, kappa = 100)
    y_target = patient.CGP 

    for i in tqdm(range(nMeals)):
        np.random.seed(int(i+75))

        Meal = np.random.uniform(5,20, size = (1,1))
        CBO = BO.contextual_bayesian_optimization(X_sample,Y_sample.reshape(-1,1),Meal,bolus_bounds,mf,
                                    kernel, X_grid = bolus_range)
        CBO.fit_gp()

        CBO.query_next_UCB()
        Bolus = CBO.X_next[0][0]

        patient.simulate(Bolus,Meal,x0 = patient.xSS, kappa = 100)
        Y_next = patient.CGP 
        xinit = patient.xEnd
        CGPI.append(patient.CGP.tolist())
        BG0.append(xinit[10])
        
        X_sample = np.vstack((X_sample, CBO.X_next))
        Y_sample = np.vstack((Y_sample, Y_next.tolist()))

        # Other things to save
        CGMi = patient.CGM
        x = np.where(CGMi<=3.9)
        y = np.where(CGMi>10)
        TAR = len(y[0])/len(CGMi)*100
        TBR = len(x[0])/len(CGMi)*100
        TIR = 100 - TAR - TBR

        # Now fix the doctor update formula
        e = (CGMi[-1] - patient.target)/patient.ISF
        patient.ICR = ((Meal * patient.ICR)/(patient.ICR * e + Meal)).squeeze()

        def prior(x):
            y = 0.06*(x[:,0]-(1/patient.ICR)*x[:,1])**2 + 3.6*patient.ICR*x[:,1]**2 
            return y.reshape(-1,1)

        mf.f = prior

        nTIR[i] = TIR
        nTBR[i] = TBR
        nMeal[i] = Meal
        nCGPi[i] = CGMi
        CGPI[i] = patient.CGP.tolist())
        BG0.append(xinit[10])

    # Save after simulation is done
    np.savez(file = path + f'data/patient{patientNr}', 
            X_sample = X_sample, 
            Y_sample = Y_sample, 
            nTIR = nTIR,  
            nTBR= nTBR, 
            nMeal = nMeal,
            nCGPi = nCGPi,
            CGPI = CGPI,
            BG0 = BG0)

    patientNr = read_lastPatient(path)

# Check if simulation is done
if simulation_is_done(path, maxPatient):
    preprocess_simulation_data(path) # preprocess output of all simulations