import numpy as np
import os
import sys
from os import walk
import shutil
from tqdm import tqdm

import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
import time
from collections import namedtuple

import seaborn as sns

sns.set_theme()

def read_env(ndim = 2):
    '''
    Function to access env variables
    '''
    # Default arguments
    path = 'debug/'
    maxPatient = 1000
    path_to_simulations = 'simulations/'
    nMeals = 200
    ndim = ndim
    loss_fun = 'CGP'
    hypoglycemia = 5
    target = 5
    patient_filter = True
    patientNr = 0
    n_callibration = 50

    jupyter = in_ipynb() # Check if we are in notebook
    
    if not jupyter:
        path_to_simulations = ''
        if len(sys.argv) > 1:
            path = sys.argv[1]
        if len(sys.argv) > 2:
            patientNr = int(sys.argv[2])
    
    # patientNr = update_lastPatient(path, maxPatient)
    env = namedtuple("env", "path maxPatient patientNr path_to_simulations nMeals ndim loss_fun jupyter hypoglycemia target patient_filter n_callibration")

    return env(path, maxPatient, patientNr, path_to_simulations, nMeals, ndim, loss_fun, jupyter, hypoglycemia, target, patient_filter, n_callibration)

def save_files(file_dict, path = None, patientNr = None):
    np.savez(file = path + f'data/patient{patientNr}', **file_dict)   

### Helper functions for simulation

def timeout():
    '''
    Function used to wait t ~ U(0,1) seconds
    Used to avoid conflicts on the cluster when simulating
    '''
    t = np.random.uniform(0,1)
    time.sleep(t)

# Functions for reading and writing the last patient
def update_lastPatient(path, maxPatient):
    timeout()
    patientNr = read_lastPatient(path) + 1
    if patientNr <= maxPatient:
        write_lastPatient(patientNr, path)
    return patientNr

def read_lastPatient(path):
    with open(path + 'lastPatient.txt') as f:
        lastPatient = f.read()
        return int(lastPatient)

def write_lastPatient(number, path):
    with open(path + 'lastPatient.txt', 'w') as f:
        f.write(str(number))

def reset_environment(path = 'debug/'):
    '''
    Function for resetting environment
    '''
    shutil.rmtree(path)
    shutil.copytree('setup/', path)

def load_data(path):
    '''
    Loads data after it has been preprocessed
    by preprocess_simulation_data
    '''
    all_data = np.load(path + 'all_data.npz')
    return all_data

def simulation_is_done(path, maxPatient):
    '''
    Check if simulation is done
    '''
    filenames = next(walk(path + 'data/', (None, None, [])))[2]
    nfiles = len(filenames)

    if nfiles == maxPatient:
        return True
    else:
        return False

def preprocess_simulation_data(path):
    '''
    Preprocess simulation data
    '''

    # load all files in output
    # Do it this way to get proper order!
    filenames = sorted(next(walk(path + 'data/', (None, None, [])))[2], 
    key = lambda x: int(x[7:-4]))
    nfiles = len(filenames)

    # Initialize total array
    all_data = dict()

    #now add each array element
    for i in tqdm(range(nfiles), desc = 'Preprocessing data of simulation'):
        dat = np.load(path + 'data/' + filenames[i])
        n_arrays = dat.files

        for array_name in n_arrays:
            # Initialize array if it has not already been added
            if array_name not in all_data.keys():
                array = dat[array_name]
                shape_array = (*array.shape, nfiles)
                all_data[array_name] = np.empty(shape_array) * np.nan # nans are used to help spot errors/collapse dimensions
                
            # Set the values of the corresponding simulation number
            all_data[array_name][...,i] = dat[array_name]

    # save output
    np.savez(path + 'all_data', **all_data)
    
    return all_data

def in_ipynb():
    try:
        cfg = get_ipython().config 
        return True
    except NameError:
        return False