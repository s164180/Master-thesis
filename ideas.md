

### Bayesian hyper parameter tuning
* Include tuning of the hyper parameter in the algorithm (as described in the papers)
* Requires A LOT OF COMPUTATIONAL POWER (Also motivation for change to Gpytorch)

### Fit prior using Bayesian Optimization
* This is probably better than setting some type of custom 
* Look up on the Dirichlet and also Beta-Gamma priors

### Update basal injection in model
* Right now we are simply adding a constant rate corresponding to the steady-state (this is why we the bloodsugar gets so high)
* Also model this using a Gaussian process (Will require considerably more computational power - i.e this is why we change the library)
* Since we have way, way more data (16128 points during the 8 weeks) we can also consider modelling this using deep learning!
  * RNN
  * LSTM 
  * Transformer
  * Reinforcement learning (see https://github.com/jxx123/simglucose)
  * Also consider combining all the data points 
* Also consider adding a discounting factor to the penaly functions. It should penalize the start more than the ending, since it appears that 
* Another approach is to combine the basal and bolus injection rate into one

### Change to Gpytorch
* More up to date library, that is integrated with PyToch and works better with the "neural network" extensions to the model
* GPU is probably needed since we wish to simualate on a much, much grander scale
* Also investigate Pyro and Botorch

### More realistic simulation
* Make the time between meals random 
  * Find a dataset and model this using an exponential distribution
* Make the meal size more realistic
  * I.e depend on the time of the day/time since last meal and the BW of the patients
* A generalized linear model should be capable of modelling this I think pretty well
* I just need to find a dataset
* Add noise to the CGM

### MPC
* Add a more complex MPC algorthm such as in the papers
* Consider Bayesian process MPC as linked on MS teams